#this is a comment in a feature file, it is a single line comment
#We are writing this in a language called Gherkin
# this is where we can translate feature behavior, into a structured english phrases 
        #that we can be used to generate test stubs and help automate our tests
@tag
Feature: Employee Submitting Reimbursement Request
		As an Employee, I wish to submit a reimbursement with an amount and a description
		
		Scenario: Successful submission of reimbursement request
		Given a user is at the reimbursement submission page
		When a user inputs an amount "<amount>"
		And a user inputs a desciption "<description>"
		But that user submits the information
		Then the user is redirected to successful submission page
		
	
#this is a comment in a feature file, it is a single line comment
#We are writing this in a language called Gherkin
# this is where we can translate feature behavior, into a structured english phrases 
        #that we can be used to generate test stubs and help automate our tests
@tag
Feature: Employee Logging in to ERS App
  As a user, I wish to login to the ERS App using proper credentials

  @tag1 # correct login
  Scenario: Successful Login to the ERS App
    Given a user is at the login page
    When a user inputs their username "<username>"
    And a user inputs their password "<pass>"
    And a user submits the information
    But that user is a user of role Employee
   	Then the user is redirected to the Employee ERS Page 

   # Examples: 
    #  | name  | value | status  |
     # | name1 |     5 | success |
      #| name2 |     7 | Fail    |


		# failed login (both inputs incorrect)
    Scenario: Failing to Login to ERS App
     Given a user is at the home page of ERS App
     When a user incorrectly inputs their username "<username>"
     And a user incorrectly inputs their password "<pass>"
     But a user submits the information
     Then the user should see the span of Enter your userName and password correct

		# failed login (username incorrect)
     Scenario: Failing to Login to ERS App 2
     Given a user is at the home page of ERS App
     When a user correctly inputs their username "<username>"
     And a user incorrectly inputs their password "<pass>"
     But a user submits the information
     Then the user should see the span of Enter your userName and password correct
     
     # failed login (password incorrect)
     Scenario: Failing to Login to ERS App 3
     Given a user is at the home page of ERS App
     When a user incorrectly inputs their username "<username>"
     And a user correctly inputs their password "<pass>"
     But a user submits the information
     Then the user should see the span of Enter your userName and password correct
      
  
      
      
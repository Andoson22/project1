#this is a comment in a feature file, it is a single line comment
#We are writing this in a language called Gherkin
# this is where we can translate feature behavior, into a structured english phrases 
        #that we can be used to generate test stubs and help automate our tests
@tag
Feature: Manager Submitting Reimbursement Decision
		As a Manager, I wish to submit a reimbursement decision with a reimbursement status
		
		Scenario: Successful submission of reimbursement decision
		Given a user is at the reimbursement decision page
		When that user approves/denies that reimbursement
		Then that reimbursement should show the updated status

      
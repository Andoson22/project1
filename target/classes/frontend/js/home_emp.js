/**
	HOME.JS GETTING SESSION USER
 */
window.onload = function() {
	console.log("js is linked");
	getInfo();
}


let table_information = [];
let pendingtable = [];
let sessUser;



//gets sessionuser function
function getSessionUser() {
	let xhttp = new XMLHttpRequest();
	xhttp.open("GET", "http://localhost:9022/user/sessuser");
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status >= 200 && xhttp.status < 300) {
			let user = JSON.parse(xhttp.responseText);
			xhttp = user;
			userInformation(user);
			console.log(user);
		}

	}

	xhttp.send();
	function userInformation(user) {
		document.getElementById('first_name').innerText = user.first_name;
		document.getElementById('last_name').innerText = user.last_name;

	}
}

	function getReimbListPend() {
	let request = new XMLHttpRequest();
	request.open("GET", "http://localhost:9022/reimbemp/pendreimb"); 
        request.onreadystatechange = function() {
			if (request.readyState == 4 && request.status >= 200 && request.status < 300) {
				let tablearray = JSON.parse(request.responseText);
				table_information = tablearray;
				console.log(table_information);
				insertReimbListPend();
			}
		}
        request.send();
	console.log("Table Information" + table_information)
}

function insertReimbListPend() {
        let table = document.getElementById("pend_re_table"); 

        for (let i = 0; i < table_information.length; i++) {
            let t = "";
            let tr = "<tr>";
            tr += "<td>"+table_information[i].reimbursement_id+"</td>";
            tr += "<td>"+table_information[i].amount+"</td>";
            tr += "<td>"+table_information[i].description+"</td>";
            tr += "<td>"+table_information[i].type_id+"</td>";
            tr += "<td>"+table_information[i].status_id+"</td>";
            tr += "<td>"+table_information[i].time_submitted+"</td>";
//            tr += "<td>"+table_information[i].time_resolved+"</td>";
            t += tr;
            table.innerHTML += t;

        }
    }
    
    	function getReimbListPast() {
	let request = new XMLHttpRequest();
	request.open("GET", "http://localhost:9022/reimbemp/pastreimb"); 
        request.onreadystatechange = function() {
			if (request.readyState == 4 && request.status >= 200 && request.status < 300) {
				let tablearray = JSON.parse(request.responseText);
				table_information = tablearray;
				console.log(table_information);
				insertReimbListPast();
			}
		}
        request.send();
	console.log("Table Information" + table_information)
}

function insertReimbListPast() {
        let table = document.getElementById("past_re_table"); 

        for (let i = 0; i < table_information.length; i++) {
            let t = "";
            let tr = "<tr>";
            tr += "<td>"+table_information[i].reimbursement_id+"</td>";
            tr += "<td>"+table_information[i].amount+"</td>";
            tr += "<td>"+table_information[i].description+"</td>";
            tr += "<td>"+table_information[i].type_id+"</td>";
            tr += "<td>"+table_information[i].status_id+"</td>";
            tr += "<td>"+table_information[i].time_submitted+"</td>";
//            tr += "<td>"+table_information[i].time_resolved+"</td>";
            t += tr;
            table.innerHTML += t;

        }
    }

async function getInfo() {
	try {
		let response = await getSessionUser();
		getReimbListPast(response);
		getReimbListPend(response);
	} catch (error) {
		console.log(error);
	}
}




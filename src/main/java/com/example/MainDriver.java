package com.example;

import java.io.File;
import java.time.LocalDate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.example.controller.ReimbursementController;
import com.example.controller.UserController;
import com.example.dao.ERSDBConnection;
import com.example.dao.ReimbursementImpl;
import com.example.dao.UserDaoImpl;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.Javalin;

public class MainDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		UserController uCon = new UserController(new UserService(new UserDaoImpl(new ERSDBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementImpl(new ERSDBConnection())));

		
//		ReimbursementImpl reimb = new ReimbursementImpl(new ERSDBConnection());
//      reimb.createNewReimbursement(150, LocalDate.now(), "Hi I'm Paul", 3, 3);
		
//		ReimbursementImpl reimb2 = new ReimbursementImpl(new ERSDBConnection());
//      reimb2.updateReimbursement(2, LocalDate.now(), 2, 2);
		
//		ReimbursementImpl reimb2 = new ReimbursementImpl(new ERSDBConnection());
//		System.out.println(reimb2.getAllReimbursementsByStatus());
		
//		ReimbursementImpl reimb2 = new ReimbursementImpl(new ERSDBConnection());
//		System.out.println(reimb2.getAllReimbursementsByAuthorid(1));
		
//		ReimbursementImpl reimb3 = new ReimbursementImpl(new ERSDBConnection());
//		System.out.println(reimb3.getPendingReimbursementsByAuthorid(1));
		
//		ReimbursementImpl reimb3 = new ReimbursementImpl(new ERSDBConnection());
//		System.out.println(reimb3.viewAllReimbursements());
		
		
//		UserDaoImpl userdaoimpl = new UserDaoImpl(new ERSDBConnection());
//		userdaoimpl.getUserByUsername("Andoson22");
//		userdaoimpl.getRoleId(userdaoimpl.getUserByUsername("Andoson22"));
		
		
		Javalin app = Javalin.create(config ->{
		config.enableCorsForAllOrigins();
		config.addStaticFiles("/frontend");
		});

		app.start(9022);
		
		app.post("/user/login", uCon.POSTLOGIN); //user login
		app.get("/user/sessuser", uCon.GETCURRENTUSER); //user login
		//EMPLOYEE
		app.post("/emp/reimbursementsubmit", rCon.CREATEREIMB); //WORKScreate new reimbursements//CONNECTS WITH HTML FORM ACTION ''
		app.get("/reimbemp/pendreimb", rCon.GETPENDINGEMP); //WORKS EMP views pending
		app.get("/reimbemp/pastreimb", rCon.GETBYAUTHOR); //WORKS EMP views past
		//MANAGER
		app.post("/ersman/update_reimb", rCon.UPDATEREIMB); //MANupdates reimbursement
		app.get("/reimbman/pastreimb", rCon.GETALLREIMB); //WORKSpast manager table
		app.get("/reimbman/pendreimb", rCon.GETBYSTATUS); //pending table manager
	
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.redirect("/html/invalidlogin.html"); //invalid login
		});
		
		
		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver(); // this will create a driver that I can now use to controller
											   // my chrome browser
		driver.get("http://localhost:9022/html/");

		
		

	}

}
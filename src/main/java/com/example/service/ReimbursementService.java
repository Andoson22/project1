package com.example.service;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.example.dao.ReimbursementImpl;
import com.example.model.Reimbursement;

public class ReimbursementService {

	private ReimbursementImpl rDao;

	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementService(ReimbursementImpl rDao) {
		super();
		this.rDao = rDao;
	}

	public Integer cTypeId(String type_id) {
		int type_Id = 0;
		if (type_id.equals("apparel")) {
			type_Id = 1;
		} else if (type_id.equals("food")) {
			type_Id = 2;
		} else if (type_id.equals("lodging")) {
			type_Id = 3;
		} else if (type_id.equals("fines")) {
			type_Id = 4;
		}
		return type_Id;
	}
	
	public Integer cStatusId(String status_id) {
		int type_Id = 0;
		if (status_id.equals("pending")) {
			type_Id = 1;
		} else if (status_id.equals("approved")) {
			type_Id = 2;
		} else if (status_id.equals("denied")) {
			type_Id = 3;
		}
		return type_Id;
	}
	//EMPLOYEE
	public void newReimb(double amount, LocalDate time_submitted, String description, int authorid, int type_id) {
		rDao.createNewReimbursement(amount, time_submitted, description, authorid, type_id);
	}
	
	public List<Reimbursement> getReimbsByAID(int authorid) {
		return rDao.getAllReimbursementsByAuthorid(authorid);
	}
	
	public List<Reimbursement> getPendingReimbursementsByAuthorid(int authorid) {
		return rDao.getPendingReimbursementsByAuthorid(authorid);
	}
	
	//MANAGER
	public void changeReimb(int rebimbursement_id, LocalDate time_resolved, int resolver_id, int status_id) {
		rDao.updateReimbursement(rebimbursement_id, time_resolved, resolver_id, status_id);
	}
	
	public List<Reimbursement> viewAllReimbs() {
		return rDao.viewAllReimbursements();
	}

	public List<Reimbursement> getReimbByStatus() {
		return rDao.getAllReimbursementsByStatus();
	}
	
	

}

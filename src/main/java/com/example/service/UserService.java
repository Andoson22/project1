package com.example.service;

import com.example.dao.UserDaoImpl;
import com.example.model.User;

public class UserService {
	
	private UserDaoImpl uDao;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public UserService(UserDaoImpl uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User checkPassword(String username, String password) {
        User user = uDao.getUserByUsername(username);
        if(user.getPass().equals(password)) {
            return user;
        }
        return null;
    }

    public User getUserbyUsername(String username) {
        User user = uDao.getUserByUsername(username);
        if(user == null) {
            return null;
        }
        return user;
    }

    public String lookforUserRole(User user) {
        if(user == null) 
            return null;

        String userRole = "";
        if(uDao.getRoleId(user) == 1) 
            userRole = "Employee";
        if(uDao.getRoleId(user) == 2)
            userRole = "Manager";
        return userRole;
    }
	
	

}

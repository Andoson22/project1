package com.example.dao;

import java.time.LocalDate;
import java.util.List;

import com.example.model.Reimbursement;

public interface ReimbursementDao {
	
	//EMPLOYEES
	void createNewReimbursement(double amount, LocalDate time_submitted, String description, int authorid, int type_id); //users can create new reimbursement
	List<Reimbursement> getAllReimbursementsByAuthorid(int authorid); //users can see their reimbursements
	List<Reimbursement> getPendingReimbursementsByAuthorid(int authorid);
	
	//MANAGERS
	void updateReimbursement(int rebimbursement_id, LocalDate time_resolved, int resolver_id, int status_id); //MANAGER update reimbursement table with decision/resolver/and time resolved
	List<Reimbursement> viewAllReimbursements(); //managers can view all
	List<Reimbursement> getAllReimbursementsByStatus(); //managers can filter reimbursements by PENDING
	

}





package com.example.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;

public class ReimbursementImpl implements ReimbursementDao {

	private ERSDBConnection dbcon;
	private ReimbursementImpl rDao;

	public ReimbursementImpl() {

	}

	public ReimbursementImpl(ERSDBConnection dbcon) {
		super();
		this.dbcon = dbcon;

	}

	@Override
	public void createNewReimbursement(double amount, LocalDate time_submitted, String description, int authorid, int type_id) {

		// TODO Auto-generated method stub
		try (Connection con = dbcon.getDBConnection()) {

			String sql = "SELECT create_reimbursement(?,?,?,?,?);";

			CallableStatement cs = con.prepareCall(sql);
			//cs.registerOutParameter(1, Types.VARCHAR);
			cs.setBigDecimal(1, BigDecimal.valueOf(amount));
			cs.setObject(2, time_submitted);  ///WHAT DO WE NEED HERE???
			cs.setString(3, description);
			cs.setInt(4, authorid);
			cs.setInt(5, type_id);
			cs.execute();
//			System.out.println(cs.getString(1));

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	@Override
	public void updateReimbursement(int reimbursement_id, LocalDate resolved, int resolver_id, int status_id) {
		// TODO Auto-generated method stub
		try (Connection con = dbcon.getDBConnection()) {
			String sql = "select change_reimbursement(?, ?, ?, ?)";
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimbursement_id); // GET THE REIMB ID
			cs.setObject(2, LocalDate.now()); // SET THE NEW STATUS
			cs.setInt(3, resolver_id); //NEED TO UPDATE THE TIME_RESOLVED AS WELL
			cs.setInt(4, status_id);
			cs.execute();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	
	
	
	
	@Override 
	public List<Reimbursement> getAllReimbursementsByAuthorid(int authorid){
		try (Connection con = dbcon.getDBConnection()) {

			String sql = "select r.reimbursement_id , r.amount , r.time_submitted , r.description, r.authorid, rs.status , rt.reimb_type "
					+ "from reimbursement r "
					+ "join users u "
					+ "on r.authorid = u.user_id "
					+ "join reimbursement_status rs "
					+ "on r.status_id = rs.status_id "
					+ "join reimbursement_type rt "
					+ "on r.type_id = rt.type_id "
					+ "where r.status_id > 1 and r.authorid = ? "
					+ "order by r.reimbursement_id desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, authorid); // SET THE NEW STATUS
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbList = new ArrayList<>();

			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), 
						rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5), 
						rs.getString(6), rs.getString(7)));
			}
			return reimbList;

		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
		return null;
		
	}
	
	@Override 
	public List<Reimbursement> getPendingReimbursementsByAuthorid(int authorid){
		try (Connection con = dbcon.getDBConnection()) {

			String sql = "select r.reimbursement_id , r.amount , r.time_submitted , r.description, r.authorid, rs.status , rt.reimb_type "
					+ "from reimbursement r "
					+ "join users u "
					+ "on r.authorid = u.user_id "
					+ "join reimbursement_status rs "
					+ "on r.status_id = rs.status_id "
					+ "join reimbursement_type rt "
					+ "on r.type_id = rt.type_id "
					+ "where r.status_id = 1 and r.authorid = ? "
					+ "order by r.reimbursement_id desc";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, authorid); // SET THE NEW STATUS
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbList = new ArrayList<>();

			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), 
						rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5), 
						rs.getString(6), rs.getString(7)));
			}
			return reimbList;

		} catch (SQLException e) {
			e.printStackTrace();
		}	
		
		return null;
		
	}
	
	
	
	@Override
	public List<Reimbursement> getAllReimbursementsByStatus() {
		// TODO Auto-generated method stub
		try (Connection con = dbcon.getDBConnection()) {

			String sql = "select r.reimbursement_id , r.amount , r.time_submitted , r.description, rs.status , rt.reimb_type "
					+ "from reimbursement r "
					+ "join users u "
					+ "on r.authorid = u.user_id "
					+ "join reimbursement_status rs "
					+ "on r.status_id = rs.status_id "
					+ "join reimbursement_type rt "
					+ "on r.type_id = rt.type_id "
					+ "where r.status_id = 1 " //PENDING
					+ "order by r.reimbursement_id desc";
			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setInt(1, authorid); // SET THE NEW STATUS
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbList = new ArrayList<>();

			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), 
						rs.getObject(3, LocalDate.class), rs.getString(4), 
						rs.getString(5), rs.getString(6)));
			}
			return reimbList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}
	
	@Override
	public List<Reimbursement> viewAllReimbursements() {
		// TODO Auto-generated method stub
		try(Connection con = dbcon.getDBConnection()){
			
			String sql = "select r.reimbursement_id , r.amount , r.time_submitted , r.description, r.authorid, rs.status , rt.reimb_type "
					+ "from reimbursement r "
					+ "join users u "
					+ "on r.authorid = u.user_id "
					+ "join reimbursement_status rs "
					+ "on r.status_id = rs.status_id "
					+ "join reimbursement_type rt "
					+ "on r.type_id = rt.type_id "
					+ "where r.status_id > 1 " //NOT PENDING
					+ "order by r.reimbursement_id desc";
					
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> reimbList = new ArrayList<>();
			
			while(rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), 
						rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5), rs.getString(6),
						rs.getString(7)));
			}
			return reimbList;
			
			
		}catch (SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
//	@Override
//	public List<Reimbursement> viewAllReimbursements() {
//		// TODO Auto-generated method stub
//		try(Connection con = dbcon.getDBConnection()){
//			
//			String sql = "select r.reimbursement_id , r.amount , r.time_submitted , r.description, r.authorid, rs.status , rt.reimb_type "
//					+ "from reimbursement r "
//					+ "join users u "
//					+ "on r.authorid = u.user_id "
//					+ "join reimbursement_status rs "
//					+ "on r.status_id = rs.status_id "
//					+ "join reimbursement_type rt "
//					+ "on r.type_id = rt.type_id "
//					+ "where r.status_id > 1 " //NOT PENDING
//					+ "order by r.reimbursement_id desc";
//					
//			PreparedStatement ps = con.prepareStatement(sql);
//			ResultSet rs = ps.executeQuery();
//			List<Reimbursement> reimbList = new ArrayList<>();
//			
//			while(rs.next()) {
//				reimbList.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), 
//						rs.getObject(3, LocalDate.class), rs.getString(4), rs.getInt(5), 
//						rs.getString(6), rs.getString(7)));
//			}
//			return reimbList;
//			
//			
//		}catch (SQLException e){
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	

}

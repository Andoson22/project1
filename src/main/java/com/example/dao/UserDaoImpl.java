package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.model.User;

public class UserDaoImpl implements UserDao{
	
	public ERSDBConnection dbcon;
	
	public UserDaoImpl() {
		
	}
	
	public UserDaoImpl(ERSDBConnection dbcon) {
		super();
		this.dbcon = dbcon;
	}
	

	@Override
	public User getUserByUsername(String username) {
		// TODO Auto-generated method stub
		try(Connection con = dbcon.getDBConnection()) {
			String sql = "select * from users where username = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,  username);
			ResultSet rs = ps.executeQuery();
			User user = null;
			
			while(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), 
						rs.getString(5), rs.getString(6), rs.getInt(7));
						
			}
			System.out.println(user);
			return user;
		} catch(SQLException e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Override
	public int getRoleId(User user) {
		// TODO Auto-generated method stub
		try(Connection con = dbcon.getDBConnection()) {
			User newUser = this.getUserByUsername(user.getUsername());
			System.out.println(newUser);
			return newUser.getUser_role_id();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	

}

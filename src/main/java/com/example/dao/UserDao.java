package com.example.dao;

import com.example.model.User;

public interface UserDao {
	
	public User getUserByUsername(String username);
	public int getRoleId(User user);
	// List<User> getAllUsers(int userId);
	
}

package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConnection {
	
	/*
	 * We need the credentials to the database to connect via JDBC
	 * the url will have the following syntax
	 * jdbc:postgresql://host-endpoint:portnumber/databasename
	 */	
	
	private static final String URL = "jdbc:postgresql://database-1.cnkwjqi8h4ti.us-east-2.rds.amazonaws.com:5432/expensesystem";
	private static final String username ="ersuser";
	private static final String password = "P4ssw0rd";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(URL, username, password);
	}

}

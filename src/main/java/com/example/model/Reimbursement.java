package com.example.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class Reimbursement {
	
	private int reimbursement_id;
	private double amount;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate time_submitted;
	private LocalDate time_resolved;
	private String description;
	private int authorid;
	private int resolverid;
	private String status_id;
	private String type_id;
	
	public Reimbursement() {
		
	}
	

	public Reimbursement(int reimbursement_id, double amount, LocalDate time_submitted, LocalDate time_resolved,
			String description, int authorid, int resolverid, String status_id, String type_id) {
		super();
		this.reimbursement_id = reimbursement_id;
		this.amount = amount;
		this.time_submitted = time_submitted;
		this.time_resolved = time_resolved;
		this.description = description;
		this.authorid = authorid;
		this.resolverid = resolverid;
		this.status_id = status_id;
		this.type_id = type_id;
	}
	
	public Reimbursement(int reimbursement_id, double amount, LocalDate time_submitted,
			String description, int authorid, String status_id, String type_id) {
		super();
		this.reimbursement_id = reimbursement_id;
		this.amount = amount;
		this.time_submitted = time_submitted;
		this.description = description;
		this.authorid = authorid;
		this.status_id = status_id;
		this.type_id = type_id;
	}
	
	public Reimbursement(int reimbursement_id, double amount, LocalDate time_submitted, LocalDate time_resolved,
			String description, int authorid, String status_id, String type_id, int resolverid) {
		super();
		this.reimbursement_id = reimbursement_id;
		this.amount = amount;
		this.time_submitted = time_submitted;
		this.time_resolved = time_resolved;
		this.description = description;
		this.authorid = authorid;
		this.status_id = status_id;
		this.type_id = type_id;
	}
	
	public Reimbursement(int reimbursement_id, double amount, LocalDate time_submitted,
			String description, String status_id, String type_id) {
		super();
		this.reimbursement_id = reimbursement_id;
		this.amount = amount;
		this.time_submitted = time_submitted;
		this.description = description;
		this.status_id = status_id;
		this.type_id = type_id;
	}
	

	public int getReimbursement_id() {
		return reimbursement_id;
	}

//	public void setReimbursement_id(int reimbursement_id) {
//		this.reimbursement_id = reimbursement_id;
//	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAuthorid() {
		return authorid;
	}

	public void setAuthorid(int authorid) {
		this.authorid = authorid;
	}

	public int getResolverid() {
		return resolverid;
	}

	public void setResolverid(int resolverid) {
		this.resolverid = resolverid;
	}

	public String getStatus_id() {
		return status_id;
	}

	public void setStatus_id(String status_id) {
		this.status_id = status_id;
	}

	public String getType_id() {
		return type_id;
	}

	public void setType_id(String type_id) {
		this.type_id = type_id;
	}

	public LocalDate getTime_submitted() {
		return time_submitted;
	}


	public void setTime_submitted(LocalDate time_submitted) {
		this.time_submitted = time_submitted;
	}


	public LocalDate getTime_resolved() {
		return time_resolved;
	}


	public void setTime_resolved(LocalDate time_resolved) {
		this.time_resolved = time_resolved;
	}



	@Override
	public String toString() {
		return "Reimbursement [reimbursement_id=" + reimbursement_id + ", amount=" + amount + ", description="
				+ description + ", authorid=" + authorid + ", resolverid=" + resolverid + ", status_id=" + status_id
				+ ", type_id=" + type_id + "]";
	}
	
	
	
	
	
	
	
	

}

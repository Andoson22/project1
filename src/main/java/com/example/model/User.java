package com.example.model;

public class User {
	
	private int user_id;
	private String username;
	private String pass;
	private String first_name;
	private String last_name;
	private String email;
	private int user_role_id;
	
	
	public User() {
		
	}


	public User(String username, String pass, String first_name, String last_name, String email,
			int user_role_id) {
		super();
		//this.user_id = user_id;
		this.username = username;
		this.pass = pass;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.user_role_id = user_role_id;
	}
	
	
	public User(int user_id, String username, String pass, String first_name, String last_name, String email,
			int user_role_id) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.pass = pass;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.user_role_id = user_role_id;
	}
	


	public int getUser_id() {
		return user_id;
	}


//	public void setUser_id(int user_id) {
//		this.user_id = user_id;
//	}


	public String getFirst_name() {
		return first_name;
	}


	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}


	public String getLast_name() {
		return last_name;
	}


	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getUser_role_id() {
		return user_role_id;
	}


	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getUsername() {
	return username;
	}


	public String getPass() {
		return pass;
	}
	
	public void setPass(String pass) {
		this.pass = pass;
	}


	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", username=" + username + ", pass=" + pass + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", email=" + email + ", user_role_id=" + user_role_id + "]";
	}
	
	
	
	
	
	

}

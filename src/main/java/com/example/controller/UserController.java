package com.example.controller;

import com.example.model.User;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;

    public UserController() {
        // TODO Auto-generated constructor stub
    }

    public UserController(UserService uServ) {
        super();
        this.uServ = uServ;
    }

    public final Handler POSTLOGIN = (ctx) -> {
        User user = uServ.checkPassword(ctx.formParam("username"), ctx.formParam("pass"));
        if(user != null) {
        	ctx.sessionAttribute("currentuser", user); // as long as the key is valid, we can access the information stored key value, we can use this to keep track of a logged in user
            if(user.getUser_role_id() == 1) {
            	ctx.redirect("/html/home_emp.html");
            }
            if (user.getUser_role_id() == 2) {// as long as the key is valid, we can access the information stored key value, we can use this to keep track of a logged in user
            	ctx.redirect("/html/home_man.html");
            } 
        } else { 
            ctx.redirect("/html/invalidlogin.html");
        }
    };

    public final Handler GETCURRENTUSER = (ctx) -> {
        User user = (User)ctx.sessionAttribute("currentuser");
        ctx.json(user);
    };
	

}

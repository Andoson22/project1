package com.example.controller;

import java.time.LocalDate;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class ReimbursementController {
	private ReimbursementService rSer;
	private UserService uSer;
	
	public ReimbursementController(ReimbursementService rSer) {
		super();
		this.rSer = rSer;
	}
	
	public final Handler CREATEREIMB = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        //System.out.println(sessuser + " is working");
        double amount = Double.parseDouble(ctx.formParam("amount"));
        LocalDate time_submitted = LocalDate.now();
        String description = ctx.formParam("description");
        int authorid = sessuser.getUser_id();
        int type_id = rSer.cTypeId(ctx.formParam("type_id"));

        rSer.newReimb(amount, time_submitted, description, authorid, type_id);
        ctx.status(200);
        ctx.redirect("/html/home_emp.html");
    };
	
	
	
	public final Handler UPDATEREIMB = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int rI = Integer.parseInt(ctx.formParam("reimbursement_id"));
        LocalDate resolved_Date = LocalDate.parse(ctx.formParam("time_resolved"));
        int resolverid = sessuser.getUser_id();
        int status_id = rSer.cStatusId(ctx.formParam("status_id"));
        rSer.changeReimb(rI, resolved_Date, resolverid, status_id);
        ctx.status(200);
        ctx.redirect("/html/home_man.html");

    };
    
    //EMPLOYEE
    public final Handler GETBYAUTHOR = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int authorid = sessuser.getUser_id();
        ctx.status(200);
        ctx.json(rSer.getReimbsByAID(authorid));

    };
    
    public final Handler GETPENDINGEMP = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int authorid = sessuser.getUser_id();
        ctx.status(200);
        ctx.json(rSer.getPendingReimbursementsByAuthorid(authorid));

    };
    
    
    //MANAGER
    public final Handler GETALLREIMB = (ctx) -> {
        ctx.status(200);
        ctx.json(rSer.viewAllReimbs());

    };
    
    public final Handler GETBYSTATUS = (ctx) -> {
        ctx.status(200);
        ctx.json(rSer.getReimbByStatus());

    };
}

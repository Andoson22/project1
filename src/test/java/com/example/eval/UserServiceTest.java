package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.UserDaoImpl;
import com.example.model.User;
import com.example.service.UserService;



public class UserServiceTest {
	
	@Mock
	private UserDaoImpl mockedDao;
	
	private UserService testService;
	
	private User testUser;
	
	@BeforeEach
	public void setUp() throws Exception{
		
		MockitoAnnotations.initMocks(this);
		
		testService = new UserService(mockedDao);
		
		testUser = new User(1, "Nandoson", "pa$$word", "Nick", "Anderson", "nickmonty@gmail.com", 1);
		
		when(mockedDao.getUserByUsername(isA(String.class))).thenReturn(testUser);
		
		when(mockedDao.getRoleId(isA(User.class))).thenReturn(1);
		
	}
	
	@Test
	public void checkPasswordSuccess() {
		String password = testUser.getPass();
		assertEquals(password, testService.getUserbyUsername("Nandoson").getPass());
	}
	
	@Test
	public void getUserbyUsernameSuccess() {
		String username = testUser.getUsername();
		assertEquals(username, testService.getUserbyUsername("Nandoson").getUsername());
	}
	
	@Test
	public void lookforUserRoleSuccess() {
		Integer userRole = testUser.getUser_role_id();
		assertEquals(userRole, testService.getUserbyUsername("Nandoson").getUser_role_id());
	}
	
	
		
}

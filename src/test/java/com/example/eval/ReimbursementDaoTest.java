package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ERSDBConnection;
import com.example.dao.ReimbursementImpl;
import com.example.model.Reimbursement;

public class ReimbursementDaoTest {
	
	@Mock
	private ERSDBConnection dbcon;
	
	@Mock
	private Connection con;
	
	@Mock 
	private PreparedStatement ps;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private ReimbursementImpl rDao;
	
	private Reimbursement reimbursement;
	
	private List<Reimbursement> reimbList;
	
	@BeforeEach
	public void setUp() throws Exception{
		
		reimbursement = new Reimbursement(1, 25, LocalDate.now(), LocalDate.now(),
				"Yerrr", 1, 1, "1", "2");
		
		reimbList = Arrays.asList(reimbursement);
		
		
		//(int reimbursement_id, double amount, LocalDate time_submitted, LocalDate time_resolved,
		///String description, int authorid, int resolverid, String status_id, String type_id) {
			
		
		MockitoAnnotations.initMocks(this);
		rDao = new ReimbursementImpl(dbcon);
		when(dbcon.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn(" Reimbursement Test Successful!!!");
		when(rs.next()).thenReturn(true).thenReturn(false);		
		when(rs.getInt(1)).thenReturn(reimbursement.getReimbursement_id());
		when(rs.getDouble(2)).thenReturn(reimbursement.getAmount());
		when(rs.getObject(3, LocalDate.class)).thenReturn(reimbursement.getTime_submitted());
		when(rs.getObject(4, LocalDate.class)).thenReturn(reimbursement.getTime_resolved());
		when(rs.getString(5)).thenReturn(reimbursement.getDescription());
		when(rs.getInt(6)).thenReturn(reimbursement.getAuthorid());
		when(rs.getInt(7)).thenReturn(reimbursement.getResolverid());
		when(rs.getString(8)).thenReturn(reimbursement.getStatus_id());
		when(rs.getString(9)).thenReturn(reimbursement.getType_id());
		
	}
	@Test
	public void createNewReimbTest() throws Exception{
		rDao.createNewReimbursement(23, null, "Fuel in the tank", 1, 1);
		verify(cs, times(1)).execute();
		
	}
	
	@Test
	public void viewAllReimbursmentsTest() {
		List<Reimbursement> reimbList = rDao.viewAllReimbursements();
		Reimbursement rList1 = reimbList.get(0);
		Reimbursement rList2 = reimbList.get(0);
		assertEquals(rList1.getReimbursement_id(), rList2.getReimbursement_id());
	}
	
	@Test
	public void viewAllReimbursementsByAuthorId() {
		List<Reimbursement> reimbList = rDao.getAllReimbursementsByAuthorid(1);
		Reimbursement rList1 = reimbList.get(0);
		Reimbursement rList2 = reimbList.get(0);
		assertEquals(rList1.getAuthorid(), rList2.getAuthorid());		
	}
	
	@Test
	public void updateReimbursement() throws SQLException {
		rDao.updateReimbursement(1, null, 1, 2);
		verify(cs, times(1)).execute();
	}
	
	
	
//	@Test
//	public void getPendingReimbursementsByAuthorid() {
//		List<Reimbursement> reimbList = rDao.getPendingReimbursementsByAuthorid(1);
//		Reimbursement rList1 = reimbList.get(0);
//		Reimbursement rList2 = reimbList.get(0);
//		assertEquals(rList1.getAuthorid(), rList2.getAuthorid());		
//	}
//	
//	@Test
//	public void getAllReimbursementsByStatus() {
//		List<Reimbursement> reimbList = rDao.getAllReimbursementsByStatus();
//		Reimbursement rList1 = reimbList.get(0);
//		Reimbursement rList2 = reimbList.get(0);
//		assertEquals(rList1.getAuthorid(), rList2.getAuthorid());		
//	}
	
	

}
package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ERSDBConnection;
import com.example.dao.UserDaoImpl;
import com.example.model.User;

public class UserDaoTest {
	
	@Mock
	private ERSDBConnection dbcon;
	
	@Mock
	private Connection con;
	
	@Mock 
	private PreparedStatement ps;
	
	@Mock
	private CallableStatement cs;
	
	@Mock
	private ResultSet rs;
	
	private UserDaoImpl uDao;
	
	private User user;
	
	
	
	@BeforeEach
	public void setUp() throws Exception{
		
		//(String username, String pass, String first_name, String last_name, String email, int user_role_id)
		user = new User(1, "Nandoson", "pa$$word", "Nick", "Anderson", "nickmonty@gmail.com", 1);
		
		MockitoAnnotations.initMocks(this);
		uDao = new UserDaoImpl(dbcon);
		when(dbcon.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("User Test Successful!!!");
		when(rs.next()).thenReturn(true).thenReturn(false);		
		when(rs.getInt(1)).thenReturn(user.getUser_id());
		when(rs.getString(2)).thenReturn(user.getUsername());
		when(rs.getString(3)).thenReturn(user.getPass());
		when(rs.getString(4)).thenReturn(user.getFirst_name());
		when(rs.getString(5)).thenReturn(user.getLast_name());
		when(rs.getString(6)).thenReturn(user.getEmail());
		when(rs.getInt(7)).thenReturn(user.getUser_role_id());
	}
	
	@Test
	public void getUserByUsernameTest() throws Exception{		
		User testUser = uDao.getUserByUsername("Nandoson");
		assertEquals(user.getUser_id(), testUser.getUser_id());
		assertEquals(user.getUsername(), testUser.getUsername());
		assertEquals(user.getPass(), testUser.getPass());
		assertEquals(user.getFirst_name(), testUser.getFirst_name());
		assertEquals(user.getLast_name(), testUser.getLast_name());
		assertEquals(user.getEmail(), testUser.getEmail());
		assertEquals(user.getUser_role_id(), testUser.getUser_role_id());
		
	}
	
	@Test
	public void getRoleId() throws Exception {
		uDao.getRoleId(user);
		assertEquals(user.getUser_role_id(), user.getUser_role_id());
	}	
	
}
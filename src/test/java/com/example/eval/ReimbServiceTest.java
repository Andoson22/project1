package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ReimbursementImpl;
import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;

public class ReimbServiceTest {	
	
	@Mock
	private ReimbursementImpl mockedDao;	
	private ReimbursementService testService;	
	private Reimbursement reimb;
	private List<Reimbursement> reimbList;
	
	@BeforeEach
	public void setUp() throws Exception{
		
		MockitoAnnotations.initMocks(this);
		
		testService = new ReimbursementService(mockedDao);
		
		reimb = new Reimbursement(1, 100, LocalDate.now(), LocalDate.now(), "Wine", 1, 1, "1", "2" );		
		
		reimbList = Arrays.asList(reimb);
		
		doNothing().when(mockedDao).createNewReimbursement(isA(Double.class), (LocalDate.now()), isA(String.class), isA(Integer.class), isA(Integer.class));
				
		doNothing().when(mockedDao).updateReimbursement(isA(Integer.class), (LocalDate.now()), isA(Integer.class), isA(Integer.class));
		
		
	}
	
	@Test
	public void newReimb1Test() throws Exception {
		testService.newReimb(0, null, null, 0, 0);
		verify(mockedDao, times(1)).createNewReimbursement(0, null, null, 0, 0);		
	}
	
	@Test
	public void updateReimbursementTest() {
		testService.changeReimb(0, null, 0, 0);
		verify(mockedDao, times(1)).updateReimbursement(0, null, 0, 0);
	}
	
	@Test
	public void viewAllReimbs() {
		List<Reimbursement> reimbList = testService.viewAllReimbs();
		int arraySize = reimbList.size();
		for(int i = 0; i < arraySize; i++) {
			//assertEquals()
		}
	}
	
	@Test
	public void viewAllReimbsTest() {
		List<Reimbursement> reimbList = mockedDao.viewAllReimbursements();
		Reimbursement rList1 = reimbList.get(0);
		Reimbursement rList2 = reimbList.get(0);
		assertEquals(rList1.getReimbursement_id(), rList2.getReimbursement_id());
	}

}